+++
date = "2017-04-15T16:23:47+02:00"
title = "Welcome!"
draft = false

+++

**Welcome everyone.**

I decided to make this blog to share with anyone who would be interested feedback on :

* what I do for living : Software Enginner in Scala/Java and everything.
* what I do for fun : [recalbox](https://github.com/recalbox) and D.I.Y. things.


You can follow me here, on [twitter/digitalumberjak](https://twitter.com/digitalumberjak), on [github/digitallumberjack](https://github.com/digitalLumberjack/) and on [gitlab.com/digitallumberjack](https://gitlab.com/digitallumberjack)

 <!--more-->

 See you soon.